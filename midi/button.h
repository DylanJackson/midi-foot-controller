#include "global_variables.h"

modes *buttonModes;
buttonFunction *functions;
extern int lastPatch;
extern int currentPatch;

class Button{
  public:
  //Variables:
    int pinNumber;    //pin location on arduino board
    bool bIsClosed = false;    //stores state of the circuit, 0 is open, 1 is closed
    int mode = 0;    //stores Mode button is in, 0 is Momentary, 1 is Latching
    bool bLatchState = false;    //stores what state a latching button is in, 0 is off, 1 is on
    bool bIsPressed = false;    //stores whether the button has been pressed or released. 0 is released, 1 is pressed
    int task; //stores what task the button will execute when triggered
    int patchNumber = 7; //stores patch number to send out

    //Constructor
    Button(int pin, int buttonTask){
      pinNumber = pin;
      task = buttonTask;
    }
    //Destructor
    ~Button(){};

  //Functions:
	//checks buttons states and runs appropiate task
    void runRoutine(){
	  //Debug information
      Serial.print("run routine fuction for pinNumber:\t");
      Serial.print(pinNumber);
      Serial.print("\n");
	  //retreives buttons energized status
      bIsClosed = (digitalRead(pinNumber)==HIGH);
	  //if button is energized but not set as pressed:
	  //set as pressed
	  //if button bahaves as latching and its latch state is false set to true
	  //and run routine
      if((bIsClosed) && (!bIsPressed)){
        Serial.print("isClosed = true, isPressed = false\n");
        bIsPressed = !bIsPressed;
        if(!bLatchState){
          bLatchState = !bLatchState;
          runTask(task);
        }
      }
	  //if button is not energized but its state is pressed:
	  //set state to not pressed and run check if the button behaves as momentary
	  //if button behaves as momentary bypass turn on clean signal
	  //if button is regular momentary return to previous patch
      else if((!bIsClosed) && (bIsPressed)){
        bIsPressed = !bIsPressed;
        Serial.print("isClosed = false, isPressed = true\n");
        switch(mode){
          case momentaryBypass:
            Serial.print("Momentary Bypass triggered\n");
            //turn to clean signal no mods.
            break;
          case momentaryLastPatch:
            Serial.print("Momentary last patch triggered\n");
            patchNumber = lastPatch;
            changePatch(patchNumber);
            break;
        }
      }
    }

    //Associates pin number with physical pin to detect changing states
    void initialize(){
      pinMode(pinNumber,INPUT);
      Serial.print(pinNumber);
      Serial.print(" initialized\n");
    }

    //changes preset buttons patch number to new patch number
    void setNewPatch(){
      Serial.print("New patch number set\n");
      patchNumber = currentPatch;
    }

    //changes preset button from latching to momentary
    void changeMode(){
      Serial.print("Latch state changed\n");
      if(mode == 0){
        mode = 1;
      }
      else{
        mode = 0;
      }
    }

  private:
    //Send patch number information out to MIDI Devices
    void sendPatchToMIDI(int patch){
      Serial.print("Send patch to MIDI function\n");
      //masterMIDI.send(ProgramChange, patchNumber, 1);
    }
    //chenges patch number
    void changePatch(int newPatch){
      Serial.print("Change Patch function\n");
      sendPatchToMIDI(newPatch);
    }
    //changes patch to patchNumber + 1
    void incrementTask(){
      Serial.print("Increment function\n");
      currentPatch = currentPatch + 1;
      changePatch(currentPatch);
    }
    //changes patch to patchNumber - 1
    void decrementTask(){
      Serial.print("Decrement function\n");
      currentPatch = currentPatch - 1;
      changePatch(currentPatch);
    }
    void bankUpTask(){
      Serial.print("bankUp function\n");
    }
    void bankDownTask(){
      Serial.print("bankdown function\n");
    }
    
    //sends buttons patch number to Midi signal.
    void presetTask(){
      Serial.print("preset function\n");
      sendPatchToMIDI(patchNumber);
    }
    
    void runTask(int buttonTask){
      Serial.print("Run task: ");
      switch(buttonTask){
        case increment:
          Serial.print("Increment task\n");
          incrementTask();
        case decrement:
          Serial.print("Decrement task\n");
          decrementTask();
        case bankUp:
          Serial.print("bank Up task\n");
          bankUpTask();
        case bankDown:
          Serial.print("bank Down task\n");
          bankDownTask();
        case preset:
          Serial.print("preset task\n");
          presetTask();
      }
    }
};
