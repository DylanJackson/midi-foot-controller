//#include <MIDI.h>
<<<<<<< HEAD
=======
#include <EEPROM.h>

#include "button.h"
#include "global_variables.h"

// Created and binds the MIDI interface to the default hardware Serial port
>>>>>>> experimental-button-class
//MIDI_CREATE_DEFAULT_INSTANCE();

using namespace std;

/*
-----Experimental EEPROM memory reading------
vector<vector<byte>> bankStorage;
int numPresetButtons = 4;
int numBanks = 10;

--------loadMemory();-----------
for(int i = 0; i < numBanks; i++){
  for(int j = 0; j < numPresetButtons; j++){
    bankStorage[i].push_back(EEPROM.read(i*numPresetButtons+j));
  }
}

--------saveMemory();-----------
for(int i = 0; i < numBanks; i++){
  for(int j = 0; j < numPresetButtons; j++){
    EEPROM.write(i*numPresetButtons+j,bankStorage[j][i]);
  }
}

--------Legacy Code?------------
void changePresetTask(&presetButton){
  presetButton.patchNumber = currentPatch;
}

*/

//Initializing buttons:
Button incrementButton(5,increment);
Button decrementButton(6,decrement);
Button bankUpButton(7, bankUp);
Button bankDownButton(8, bankDown);
Button presetOneButton (9, preset);
Button presetTwoButton(10, preset);
Button presetThreeButton(11, preset);
Button presetFourButton(12, preset);
//Adding buttons to an array.
Button* buttonArray[] = {&incrementButton, &decrementButton, &bankUpButton, &bankDownButton, 
                &presetOneButton, &presetTwoButton, &presetThreeButton, &presetFourButton};

void setup() {
  for(int i = 0; i < sizeof(buttonArray)/sizeof(buttonArray[0]); i++){
    buttonArray[i]->initialize();
  }
  Serial.begin(9600);
  //Serial.begin(31250);
  //MIDI.begin(MIDI_CHANNEL_OMNI);
}

// put your main code here, to run repeatedly:
void loop() {

  //check if multiple buttons are pressed
  Button* multipleArray[2];
  int count = 0;
  for(int i = 0; i < sizeof(buttonArray)/sizeof(buttonArray[0]); i++){
    if((buttonArray[i]->bIsClosed) && (count < 3)){
      multipleArray[count] = buttonArray[i];
      count = count + 1;
    };
  }
  //take appropiate action for multiple button presses
  if((multipleArray[0]->task == preset) && (multipleArray[1]->task == increment)){
    multipleArray[0]->setNewPatch();
  }
  if((multipleArray[1]->task == preset) && (multipleArray[0]->task == increment)){
    multipleArray[1]->setNewPatch();
  }
  if((multipleArray[0]->task == preset) && (multipleArray[1]->task == decrement)){
    multipleArray[0]->changeMode();
  }
  if((multipleArray[1]->task == preset) && (multipleArray[0]->task == decrement)){
    multipleArray[1]->changeMode();
  }


  //for each button, check its status and run appropiate task if pressed.
  for(int i = 0; i < sizeof(buttonArray)/sizeof(buttonArray[0]); i++){
    buttonArray[i]->runRoutine();
    delay(1000);
  }
}
