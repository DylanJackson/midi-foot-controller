# Midi Foot Controller
Source code for a Midi Operated Foot Controller

increment task:
	changes to the next patch up
decrement task:
	changes to the next patch down
bankUp patch:
bankDown patch:

preset task:
	changes to the preset buttons saved patch number
setNewPatch task:
	changes a preset buttons patchNumber to currentPatch
changeMode:
	changes the latching state of a preset button
	
	
pressing a patch button and increment will change the patch buttons stored patch to current patch
pressing a patch button and decrement will change the latching mode